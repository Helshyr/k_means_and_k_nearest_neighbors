# -*- coding: utf-8 -*-
#___________________________________________________________________________________________________________

## imports__________________________________________________________________________________________________

from math import sqrt
from random import randint,choice,uniform


##functions_________________________________________________________________________________________________

def distance(p1,p2):
    """
    distance entre deux points
    entree:tuple,tuple
    sortie: float
    """
    return sqrt(((p2[0]-p1[0])**2)+((p2[1]-p1[1])**2))


def init_centroides(n):
    """
    fonction qui retourne une liste de  couples aleatoires qui n'est pas dans g
    entree:int
    sortie:list ( avec tuples)
    """
    liste_x=[]
    liste_y=[]
    for (x,y) in g:
        liste_x.append(x)
        liste_y.append(y)
    max_x=max(liste_x)
    min_x=min(liste_x)
    max_y=max(liste_y)
    min_y=min(liste_y)
    new_max_x=max_x-(max_x-min_x)/4
    new_min_x=min_x+(max_x-min_x)/4
    new_max_y=max_y-(max_y-min_y)/4
    new_min_y=min_y+(max_y-min_y)/4
    liste_couples=[]
    for i in range(n):
        liste_couples.append((uniform(new_min_x,new_max_x),uniform(new_min_y,new_max_y)))
    return liste_couples

"""
    liste_couples=[]
    for i in range(n):
        liste_couples.append(choice(g))
    return liste_couples
"""
def plus_proche_centroides(point,liste_centroides):
    """
    fonction qui retourne l'indice du couple dans la liste centroides le plus proche du point
    entree: tuple,list
    sortie: int
    """
    liste_dist = []
    for couple in liste_centroides:
        dist = liste_dist.append(distance(point,couple))
    return liste_dist.index(min(liste_dist))

def assignation(liste_points,liste_centroides):
    """
    fonction qui créer une liste de liste de points, où chaque centroide à son groupe
    (sa liste de point les plus proches associés)
    l'indice des elements de la liste_assignation en sortie correspond à l'indice du centroide associé
    dans liste_centroides

    entree:list ( contient tuple),list (contient tuple)
    sortie: list ( qui contient des list de tuple)

    """
    liste_assignation=[]
    for i in range(len(liste_centroides)):
        liste_assignation.append([])
    for point in liste_points:
        i_cent_proche=plus_proche_centroides(point,liste_centroides)
        liste_assignation[i_cent_proche].append(point)
    return liste_assignation

def barycentre(l):
    """
    fonction qui fait la moyenne des abcisses et ordonnées de la liste des points l et retourne le barycentre
    entree:list ( qui contient des tuples)
    sortie:tuple
    """
    moy_x=0
    moy_y=0
    for (x,y) in l:
        moy_x+=x
        moy_y+=y
    return (moy_x/len(l),moy_y/len(l))

def calibrage(l_l_points):
    """
    fonction qui retourne la liste des barycentres de chaque liste de l_l_points
    entree:list ( qui contient des list de tuple)
    sortie:list
    """
    liste_barycentre=[]
    for liste_pts in l_l_points:
        liste_barycentre.append(barycentre(liste_pts))
    return liste_barycentre

def k_means(liste_de_pts,nbr_g):
    l_cent_ancien=[]
    l_centroides=init_centroides(nbr_g)
    while l_cent_ancien!=l_centroides:
        l_l_groupes=assignation(liste_de_pts,l_centroides)
        #print("l_l_groupes",l_l_groupes)
        for i in range(nbr_g):
            while l_l_groupes[i]==[]:
                l_centroides[i]=(uniform(0,20),uniform(0,20))
                l_l_groupes=assignation(liste_de_pts,l_centroides)
        l_cent_ancien=l_centroides
        l_centroides=calibrage(l_l_groupes)
    return l_l_groupes


def in_grp(point1,point2,grp_ini):
    if point1 in grp_ini and point2 in grp_ini:
        return True
    else:
        return False


def score_partition(partition_initiale, partition_kmeans):
    score=0
    for grp in partition_kmeans:
        for i in range(len(grp)):
            for j in range(i+1,len(grp)):
                if in_grp(grp[i],grp[j],partition_initiale):
                    score+=1
    return score


##main_________________________________________________________________________________________________________

g1 = [(5, 3), (5, 6), (5, 0), (4, 3), (0, 3), (2, 4), (5, 5), (6, 2), (2, 2), (4, 4)]
g2 = [(9, 10), (10, 10), (11, 11), (11, 10), (8, 12), (13, 10), (10, 9), (8, 8), (12, 10), (9, 9)]
g3 = [(10, 1), (9, 3), (8, 4), (7, 2), (11, 1), (9, 2), (12, 3), (10, 1), (9, 4), (10, 3)]
g4 = [(4, 17), (5, 14), (4, 15), (5, 13), (4, 13), (7, 13), (6, 12), (6, 14), (4, 12), (4, 17)]

g = []
for g_i in [g1, g2, g3, g4]:
    for p in g_i :
        g.append(p)


print("résultat de k means : ",k_means(g,4))

#print(in_grp((10,1),(9,3),[(10,1),(9,3),(8,4),(7,2),(11,1),(9,2),(12,3),(10,1),(9,4),(10,3)]))
#print(in_grp((10,1),(9,3),[(9,10),(10,10),(11,11),(11,10),(8,12),(13,10),(10,9),(8,8),(12,10),(9,9)]))


#print(pts_in((9,3),g2))

#for i in range(200):
#   print(score_partition([g1,g2,g3,g4],k_means(g,4)))

print(score_partition(g,k_means(g,4)))
