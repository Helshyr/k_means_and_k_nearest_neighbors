# -*- coding: utf-8 -*-

"""
MASSE Oceane
ROUSSEAU Eûrus
HERMAND Maeva

Here you have our version of KNN.
PS : You will have to change the path for jeu_4d and Iris of Fisher.
PSS: Iris of fisher has a hidden caracter instead of a space ( it's an "_"). I had to remove it in Emacs.
PSSS: Nothing. 
"""

##imports___________________________________________________________________________________________________________

from math import sqrt
import copy
import time
import random

##functions__________________________________________________________________________________________________________

def euclidean_distance(p1, p2):
    """
    distance between two points in euclidean n-space
    in:tuple, tuple
    out:float
    """
    if len(p1) != len(p2):
        raise Warning("dim p1 and p2 are different")
    else:
        s = 0
        for a,b in zip(p1,p2):
            s += (a-b)**2
    return sqrt(s)

def nn(training_point, training_labels, point): # nn stands for Nearest_Neighbours
    """
    function that returns a tuple with the nearest point of var point and the index of the list in l_l_points in wich he belongs
    in: list (of list), list,list
    out: tuple (of list, int)
    """
    min_dist = euclidean_distance(training_point[0], point)
    for i, p in enumerate(training_point):
        new = euclidean_distance(p, point)
        if new <= min_dist:
            min_dist = new
            (n_p, g) = (p, training_labels[i]) #n_p stands for Nearest_Point ; g for Group
    return (n_p, g) # tuple with the nearest point and his groupe

def knn(training_point, training_labels, point, k):
    """
    function that returns the group, regarding the k-nearest neighbours
    in: list (of list), list, int, list, int
    out: int
    """
    point_copie = copy.deepcopy(training_point)
    labels_copie = copy.deepcopy(training_labels)
    k_list = [] # list of my k nearest point of var point with their groups
    for i in range(k):
        n_p_g = nn(point_copie, labels_copie, point) # n_p_g Nearest_Point_Groupe; tuple
        k_list.append(n_p_g)
        point_copie.remove(n_p_g[0]) # i delete the nearest point in labels_copie
        labels_copie.remove(n_p_g[1])
    count = {} # futur dict with the index as the key and the occurence of the index as the value
    for t in k_list:
        count[t[1]] = count.get(t[1], 0) + 1
    ordered_count = sorted(count.items(), reverse = True, key = lambda c:c[1]) # I order the list from count.item() ( the list with (key,value) )according to the value ( so the occurence of the index), in diminishing order
    return ordered_count[0][0] # represent his group

def knn_list(training_point, training_labels, test_point, k):
    """
    function that returns the labels list of the test points
    in:list, list, list, int
    out:list
    """
    answer=[]
    for p in test_point:
        answer.append(knn(training_point, training_labels, p, k))
    return answer

def score(result_of_knn_list, test_labels):
    """
    function that returns a score btw 0 and 1 by comparing results of knn
    in: list, list
    out: float
    """
    if len(result_of_knn_list) != len(test_labels):
        raise Warning("the len of the list in argument are different")
    score = 0
    for a, b in zip(result_of_knn_list, test_labels):
        if a == b:
            score += 1
    return score/len(result_of_knn_list)

def best_k(training_point, training_labels, dev_point, dev_labels, k_max):
    """
    functions that find the best k in order to have the highest score
    in: list, list, list, list, int
    out: int
    """
    max_score = 0
    k = 0
    for i in range(1, k_max + 1):
        #print("i",i)
        res_list = knn_list(training_point, training_labels, dev_point, i)
        res_score = score(res_list, dev_labels)
        #print("res list",res_list)
        #print("res_score",res_score)
        #print("max score",max_score)
        if res_score > max_score:
            max_score = res_score
            k = i
            #print("k",k)
            #print("max score apres",max_score)
    return k


##jeu_2d_____________________________________________________________________________________________________

points_train=[[2,5],[1,7],[1,10],[2,6],[5,7],[3,6],[1,9],[1,10],[4,9],[1,10],[3,10],[5,8],[0,8],[5,5],[4,10],[0,10],[0,5],[4,6],[0,9],[5,6],[19,0],[20,4],[16,5],[17,3],[15,0],[17,4],[16,3],[15,3],[16,3],[17,1],[15,4],[18,4],[18,3],[20,5],[16,5],[20,2],[17,5],[20,2],[15,2],[19,4],[14,16],[12,20],[14,19],[12,20],[12,20],[13,18],[11,17],[14,17],[11,15],[12,20],[13,17],[11,18],[13,18],[12,15],[13,15],[12,20],[10,20],[13,17],[15,15],[11,17],[7,13],[10,15],[9,10],[9,11],[8,14],[9,10],[9,13],[7,12],[8,11],[10,15],[5,12],[10,11],[10,15],[10,12],[5,12],[5,11],[10,11],[10,13],[8,13],[8,11]]
labels_train = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4]

points_test = [[2,8],[2,7],[1,10],[2,5],[2,5],[16,0],[17,5],[15,0],[17,5],[20,5],[14,15],[11,15],[13,20],[10,15],[11,18],[9,11],[8,10],[7,15],[6,12],[9,14]]
labels_test = [1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,4,4,4,4,4]

#training set separation

train_p_1 = []
train_l_1 = []
dev_p_1 = []
dev_l_1 = []

dico = {}
for p, l in zip(points_train, labels_train):
    dico[l] = dico.get(l, [])+[p]
for key, value in dico.items():
    random.shuffle(value)
    train_p_1.extend(value[:int(len(value) * 0.8)])
    train_l_1.extend([key] * int(len(value) * 0.8))
    dev_p_1.extend(value[int(len(value) * 0.8):])
    dev_l_1.extend([key] * int(len(value) * 0.2))

##results of jeu_2d_____________________________________________________________________________________________

print("_______Jeu_2d in process please wait: less 1s (for k_max=10)_______\n")

start_time = time.time()

#best k in the dev set
the_best_k = best_k(train_p_1, train_l_1, dev_p_1, dev_l_1, 10)
print("best k jeu_2d : ", the_best_k)

#res of knn_list for point_test
res_for_best_k = knn_list(train_p_1, train_l_1, points_test,the_best_k)
print(f"res of test point for best k={the_best_k} for jeu_2d : ",res_for_best_k)

#score of res of knn_list
score_for_best_k = score(res_for_best_k, labels_test)
print(f"score of knn for best k={the_best_k} for jeu_2d : ", score_for_best_k)


print("Process time : %s s ---" % (time.time() - start_time),"\n")


##jeu_4d__________________________________________________________________________________________________________
#ps : I know that crazy way to extract data, i will think about pandas and numpy next time

with open("/home/oceane/Bureau/Cours/info/L1/S2/heirate_mich/data_4d.csv") as data_file:
    count = 0
    train_p_2 = []
    train_l_2 = []
    dev_p_2 = []
    dev_l_2 = []
    test_p_2 = []
    test_l_2 = []
    for line in data_file:
        count += 1
        line = line.strip()
        list_line = line.split(",")
        if count <= 400:
            if count < 300:
                train_p_2.append([float(list_line[0]), float(list_line[1]), float(list_line[2]), float(list_line[3])])
                train_l_2.append(int(list_line[4]))
            else:
                dev_p_2.append([float(list_line[0]), float(list_line[1]), float(list_line[2]), float(list_line[3])])
                dev_l_2.append(int(list_line[4]))
        elif count <= 500:
            test_p_2.append([float(list_line[0]), float(list_line[1]), float(list_line[2]), float(list_line[3])])
            test_l_2.append(int(list_line[4]))
            if count == 500:
                count=0

##res_jeu_4d_________________________________________________________________________________________________________________


print("_______jeu_4d in process please wait: less 4min (for k_max=15) ___________\n")

start_time = time.time()

#best k in the dev set
the_best_k = best_k(train_p_2, train_l_2, dev_p_2, dev_l_2, 15)
print("best k jeu_4d : ", the_best_k)

#res of knn_list for point_test
res_for_best_k=knn_list(train_p_2, train_l_2, test_p_2, the_best_k)
print(f"res of test point for best k={the_best_k} for jeu_4d : ", res_for_best_k)

#score of res of knn_list
score_for_best_k=score(res_for_best_k, test_l_2)
print(f"score of knn for best k={the_best_k} for jeu_4d : ", score_for_best_k)



print("Process time : %s s ---" % (time.time() - start_time),"\n")

## Iris of Fisher__________________________________________________________________________________
#ps : I know that crazy way to extract data, i will think about pandas and numpy next time

with open("/home/oceane/Bureau/Cours/info/L1/S2/heirate_mich/iris1.csv") as data_file:
    count = 0
    train_p_3 = []
    train_l_3 = []
    dev_p_3 = []
    dev_l_3 = []
    test_p_3 = []
    test_l_3 = []
    for line in data_file:
        count += 1
        line = line.strip()
        list_line = line.split(",")
        if count <= 40:
            if count < 30:
                train_p_3.append([float(list_line[0]), float(list_line[1]), float(list_line[2]), float(list_line[3])])
                train_l_3.append(list_line[4])
            else:
                dev_p_3.append([float(list_line[0]), float(list_line[1]), float(list_line[2]), float(list_line[3])])
                dev_l_3.append(list_line[4])
        elif count <= 50:
            test_p_3.append([float(list_line[0]), float(list_line[1]), float(list_line[2]), float(list_line[3])])
            test_l_3.append(list_line[4])
            if count == 50:
                count=0


## res of Iris of Fisher________________________________________________________________________________
print("_______iris data in process please wait: less 2s (for k_max=10) ___________\n")

start_time = time.time()

#best k in the dev set
the_best_k = best_k(train_p_3, train_l_3, dev_p_3, dev_l_3, 10)
print("best k iris : ", the_best_k)

#res of knn_list for point_test
res_for_best_k=knn_list(train_p_3, train_l_3, test_p_3, the_best_k)
print(f"res of test point for best k={the_best_k} for iris : ", res_for_best_k)

#score of res of knn_list
score_for_best_k=score(res_for_best_k, test_l_3)
print(f"score of knn for best k={the_best_k} for iris : ", score_for_best_k)



print("Process time : %s s ---" % (time.time() - start_time),"\n")
